# tuple instead of list!
seqs = ( "AAAATGCGCGTTATACAG",
         "TATAATGCGCGCGGGGATACAG",
         "CGCGCATGATATATGCGTATATA",
       )

# print out the sequences
for seq in seqs:
    print(seq)