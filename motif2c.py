seqs = "AAAATGCGCGTTATACAG", \
         "TATAATGCGCGCGGGGATACAG", \
         "CGCGCATGATATATGCGTATATA"

motif     = "ATG"
prefixlen = 10

header = ' '*prefixlen + '*'*len(motif)
sepno  = 2 # bigger number if there are more records
lineno = 0

for seq in seqs:
    if lineno%sepno == 0:
        print(header)
    print(' '*(prefixlen-seq.find(motif))+seq)
    lineno = lineno + 1
print(header)
    
