seqs = "AAAATGCGCGTTATACAG", \
         "TATAATGCGCGCGGGGATACAG", \
         "CGCGCATGATATATGCGTATATA"

motif     = "ATG"
prefixlen = 10

for seq in seqs:
    p1, p2 = seq.split(motif, 1)
    print(p1.rjust(prefixlen), motif, p2)