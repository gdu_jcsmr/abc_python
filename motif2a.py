seqs = "AAAATGCGCGTTATACAG", \
         "TATAATGCGCGCGGGGATACAG", \
         "CGCGCATGATATATGCGTATATA"

motif     = "ATG"
prefixlen = 10

header = ' '*prefixlen + '*'*len(motif)

print(header)
for seq in seqs:
    print(' '*(prefixlen-seq.find(motif))+seq)
print(header) # indented or not?

