motifpairs = ((seq, mre.search(seq)) for seq in seqs)
seqxs = [(seq, m.start(), m.end()) for seq, m in motifpairs if m]
plen = max(s for seq, s, f in seqxs)
plen = max(f-s for seq, s,f in seqxs)
for seq, s, f in seqxs:
    prefix = seq[:s].rjust(plen)
    motif =  seq[s:f].ljust(mlen)
    print(prefix, motif, seq[f:])

