import datetime

class sample:
    def age(self):
        return datetime.date.today() - self.sdate

    def __init__(self, id, year, month, day):
        self.ID = id
        self.sdate = datetime.date(year, month, day)

    def __str__(self):
        result = 'ID:', self.ID.rjust(6), '  date=', self.sdate, 'age=', self.age()
        return ' '.join(map(str, result))
        
s1 = sample("40141", 2017, 9, 27)
print(s1)

