filename  = "seqs.txt"
import re
def main(fn, motif):
    mre = re.compile(motif)
    def mprep(lx):
        s = lx.strip()
        m = mre.search(s)
        return (s, m.start(), m.end()) if m else None
    
    with open(fn) as srcfile:
        splitseqs = list(filter(bool, map(mprep, srcfile)))
    
    plen = max(s   for seq, s, f in splitseqs)
    mlen = max(f-s for seq, s, f in splitseqs)    
    for seq, s, f in splitseqs:
        print(seq[:s].rjust(plen), seq[s:f].ljust(mlen), seq[f:])
    return
    
if __name__=="__main__":
    main(filename, "ATG")

