import math

def quadsol(a, b, c):
    sqrtpart = b**2 - 4*a*c
    assert sqrtpart>=0, 'no real solution'
    sqrtpart = math.sqrt(sqrtpart)
    return (b-sqrtpart)/(a+a), (b+sqrtpart)/(a+a)
     
a, b, c = 1.5, 5.0, 3.2

s1, s2 = quadsol(a, b, c)
print("solutions:", s1, s2)
