seqs = "AAAATGCGCGTTATACAG", \
         "TATAATGCGCGCGGGGATACAG", \
         "CGCGCATGATATATGCGTATATA"

motif     = "ATG"

def motiflen(s):
    global motif
    return s.find(motif)
    
def printmotif(s, m, k):
    p1, p2 = s.split(m, 1)
    print(p1.rjust(k), m, p2)
    return

seqlens = map(motiflen, seqs)
prefixlen = max(seqlens)
for seq in seqs:
    printmotif(seq, motif, prefixlen)
