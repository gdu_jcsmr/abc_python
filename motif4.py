seqs = "AAAATGCGCGTTATACAG", \
         "TATAATGCGCGCGGGGATACAG", \
         "CGCGCATGATATATGCGTATATA"

motif     = "ATG"
prefixlen = 0

for seq in seqs:
    p1, p2 = seq.split(motif, 1)
    prefixlen = max(prefixlen, len(p1))

for seq in seqs:
    p1, p2 = seq.split(motif, 1)
    print(p1.rjust(prefixlen), motif, p2)
