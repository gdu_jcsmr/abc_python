import datetime

class person:
    def age(self):
        return datetime.date.today() - self.dob

me = person()
me.name = "Bob"
me.dob = datetime.date(1990, 4, 1)

cam = person()
cam.name = "Cameron"
cam.dob = datetime.date(1995, 1, 1)

for p in [me, cam]:
    print(p.name.ljust(10), "age =", 
          p.age().days//365, 'years', 
          p.age().days%365//30, 'months' )
