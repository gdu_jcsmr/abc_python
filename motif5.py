seqs = "AAAATACGCGTTATACAG", \
         "TATAATGCGCGCGGGGATACAG", \
         "CGCGCATGATATATGCGTATATA"

motif     = "ATG"
prefixlen = 0

for seq in seqs:
    plen = seq.find(motif)
    # detect a problem and exit gracefully
    assert not plen<0, 'No motif in sequence: '+seq
    prefixlen = max(prefixlen, plen)

for seq in seqs:
    p1, p2 = seq.split(motif, 1)
    print(p1.rjust(prefixlen), motif, p2)
