seqs = "AAAATGCGCGTTATACAG", \
         "TATAATGCGCGCGGGGATACAG", \
         "CGCGCATGATATATGCGTATATA"
         
motif     = "ATG"
prefixlen = 10

for seq in seqs:
    hdrlen = seq.find(motif)
    spaces = ' '*(prefixlen-hdrlen)
    result = spaces + seq
    print(result)
