import collections

fn = 'seqs.txt'
def kmers(sraw, k):
    s = sraw.rstrip()
    return (s[i:i+k] for i in range(len(s)-k+1))
with open(fn) as src:
    ks = (x for r in src for x in kmers(r, 3))
    cx = collections.Counter(ks)
for k, cnt in cx.most_common(5):
    print(k, '-', cnt)
