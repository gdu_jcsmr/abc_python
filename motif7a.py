seqs = "AAAATGCGCGTTATACAG", \
         "TATAATGCGCGCGGGGATACAG", \
         "CGCGCATGATATATGCGTATATA"

# motifpattern     = r"TAA|TAG|TGA" # stop codon
motifpattern = 'A+T'

import re
motifre = re.compile(motifpattern)

plen, mlen, splitseqs = 0, 0, []

# find length of prefix and motif
# make splitseqs a list of sequences split into parts
for seq in seqs:
    m = motifre.search(seq)
    if not m:
        continue
    p1, p2 = seq.split(m.group(), 1)
    plen, mlen = max(plen, m.start()), max(mlen, m.end()-m.start())
    seqsplit = (p1, m.group(), p2)
    splitseqs.append(seqsplit)

# print the list of parts - align the parts
for p1, m, p2 in splitseqs:
    print(p1.rjust(plen), m.center(mlen), p2)
