seq = "AAAATGCGCGTTATACAG" + \
         "TATAATGCGCGCGGGGATACAG" + \
         "CGCGCATGATATATGCGTATATA"

ksize = 3
kdict = {}

# count kmers in dictionary
knum = len(seq)-ksize+1
print(knum, str(ksize)+'-mers in sequence')
for i in range(knum):
    kmer = seq[i:i+ksize]
    if kmer in kdict:
        kdict[kmer] += 1
    else:
        kdict[kmer] = 1
print(len(kdict), 'distinct %d-mers'%ksize)

# find the most frequent k-mer(s)        
kmax, mcnt = max(kdict.values()), 0
for k, v in kdict.items():
    if v==kmax:
        print('most common', str(ksize)+'-mer is:', k)
        mcnt += 1
print('They occur' if mcnt>1 else 'It occurs', kmax, 'times.')