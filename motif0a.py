seq1="AAAATGCGCGTTATACAG"
seq2="TATAATGCGCGCGGGGATACAG"
seq3="CGCGCATGATATATGCGTATATA"

# print out the sequences
for seq in [seq1, seq2, seq3]:
    print(seq)
