import collections

Fastqx = collections.namedtuple('Fastqx', ['ID', 'seq', 'qual'])

class Fastq(Fastqx):
    def __str__(self):
        result = '@'+self.ID, self.seq, '+', self.qual
        return '\n'.join(result)
        
def fq_reader(file):
    lines = (lx.strip() for lx in file)
    records = zip(lines, lines, lines, lines)
    return (Fastq(h[1:], s, q) for h, s, x, q in records)
    
with open('seqs.fq') as srcfile:
    for fq in fq_reader(srcfile):
        if fq.seq.startswith('AT') and not fq.qual.startswith('@'):
            print(fq)

