filename  = "seqs.txt"
motif     = "ATG"
import re

def main(fn, mp):
    motifre = re.compile(mp)
    
    with open(fn) as srcfile:
        plen, mlen, splitseqs = 0, 0, []    
        # find length of prefix and motif, make splitseqs list
        for srec in srcfile:
            seq = srec.rstrip() 
            m   = motifre.search(seq)
            if m:
                plen, mlen = max(plen, m.start()), max(mlen, m.end()-m.start())
                p1, p2 = seq.split(m.group(), 1)
                splitseqs.append( (p1, m.group(), p2) )
    
    # print the list of parts - align the parts
    for p1, m, p2 in splitseqs:
        print(p1.rjust(plen), m.ljust(mlen), p2)
    return
        
main(filename, motif)
