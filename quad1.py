
def quad(x, a, b, c):
    result = a*x**2 + b*x + c
    return result

# a, b, c = 1.5, -5.0, 3.2

for z in [0, 1, 2, 3, 4]:
    print(z, quad(z, 1.5, -5.0, 3.2))
