seqs = "AAAATGCGCGTTATACAG", \
         "TATAATGCGCGCGGGGATACAG", \
         "CGCGCATGATATATGCGTATATA"

motif     = "ATG"
prefixlen = 10

header = ' '*prefixlen + '*'*len(motif)
sepno  = 2 # bigger number if there are more records

for lineno, seq in enumerate(seqs):
    if (lineno%sepno)==0:
        print(header)
    print(' '*(prefixlen-seq.find(motif))+seq)
print(header)
    
