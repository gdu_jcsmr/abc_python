seqs = "AAAATGCGCGTTATACAG", \
         "TATAATGCGCGCGGGGATACAG", \
         "CGCGCATGATATATGCGTATATA"
         
motif     = "ATG"
prefixlen = 10

# align mnotis in the sequences
for seq in seqs:
    # calculate the number of leading spaces
    n = prefixlen-seq.find(motif)
    assert n>=0 and n<=10
    print(' '*n+seq)