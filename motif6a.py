seqs = "AAAATGCGCGTTATACAG", \
         "TATAATGCGCGCGGGGATACAG", \
         "CGCGCATGATATATGCGTATATA"

motif     = "ATG"

def getlen(ss, m):
    maxlen = 0
    
    for s in ss:
        plen = s.find(m)
        assert not plen<0, 'No motif in sequence: '+s
        maxlen = max(maxlen, plen)
    return maxlen
    
def motiffmt(s, m, k):
    p1, p2 = s.split(m, 1)
    parts = p1.rjust(k), m, p2
    return ' '.join(parts)

prefixlen = getlen(seqs, motif)
for seq in seqs:
    print(motiffmt(seq, motif, prefixlen))
